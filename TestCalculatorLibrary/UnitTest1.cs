﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorLibrary;
using System.Numerics;
using System.Text;

namespace TestCalculatorLibrary
{
    [TestClass]
    public class UnitTest1
    {
        private static StringBuilder BigString1 = new StringBuilder();
        private static StringBuilder BigString2 = new StringBuilder("(1+1)", 1000000);

        static UnitTest1()
        {
            //Initialize BigString1
            BigString1.Append(File.ReadAllText("BigString.txt"));

            //Initialize BigString2
            for (int i = 0; i < 1000000 / 100; i++)
                BigString2.Append("+(((375293559706-375259523456+5+1)/29911385)*3561-2546+352684*255511)/10+333598/33365+99-10000+9999");
        }


        [TestMethod]
        public void TestSum()
        {
            Assert.AreEqual(7217, Answer("124+2635+124+568+(234+3532)"));
        }

        [TestMethod]
        public void TestDifference()
        {
            Assert.AreEqual(339630623, Answer("341151431-13461-1462457-83562-(34575-73247)"));
        }

        [TestMethod]
        public void TestMultiplication()
        {
            Assert.AreEqual(743758080, Answer("12*634*235*(52*8)"));
        }

        [TestMethod]
        public void TestDivision()
        {
            Assert.AreEqual(4, Answer("25351222/4623/68/(54/3)"));
        }

        [TestMethod]
        public void TestCalculate()
        {
            Assert.AreEqual(30, Answer("(2+3)*6"));
            Assert.AreEqual(20, Answer("2+3*6"));
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void TestDivideByZeroException()
        {
            Answer("23412/0");
            Answer("63452/(12-(1376*0+14-2))");
            Answer("2+4+235-14/(30-17-13)");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void TestFormatException()
        {
            Answer("");
            Answer("null");
            Answer("srtjtyd");
            Answer("           ");
            Answer("__________________");
            Answer("5=x");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestArgumentOutOfRangeException()
        {
            Answer(new string('1', 9999999));
        }

        [TestMethod]
        [Timeout(60000)]
        public void TestLongTime()
        {
            Assert.AreEqual(-2648600, Answer(BigString1.ToString()));
        }

        [TestMethod]
        [Timeout(60000)]
        public void TestLongTime2()
        {
            Assert.AreEqual(90114643600002, Answer(BigString2.ToString()));
        }

        
        public BigInteger Answer(string s)
        {
            return Calculator.Calculate(s); 
        }
    }
}
