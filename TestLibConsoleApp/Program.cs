﻿using System;
using CalculatorLibrary;

namespace TestLibConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(
                    Calculator.Calculate(
                        Console.ReadLine()
                        )
                    );
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
